//
//  Desafio99Tests.swift
//  Desafio99Tests
//
//  Created by Guilherme Castro on 9/11/15.
//  Copyright © 2015 Guilherme Castro. All rights reserved.
//

import XCTest
import CoreLocation
import OHHTTPStubs

@testable import Desafio99

class TaxiManagerTests: XCTestCase, TaxiManagerDelegate {
    
    var expectation: XCTestExpectation?
    var finishRequestClosure: ([Taxi] -> Void)?
    
    override func setUp() {
        super.setUp()
        expectation = self.expectationWithDescription("Wait for API response")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetTaxisLocationsWithSuccess() {
        self.finishRequestClosure = { (taxis:[Taxi]) -> Void in
            
            // assert
            
            XCTAssertEqual(taxis.count, taxis.count, "Expected not an empty list")
            XCTAssertEqual(taxis[0].driverId, 5997, "Unexpected driverId")
            XCTAssertEqual(taxis[0].latitude, -23.60810717, "Unexpected latitude")
            XCTAssertEqual(taxis[0].longitude, -46.67500346, "Unexpected longitude")
            XCTAssertEqual(taxis[0].driverAvailable, true, "Unexpected driverAvailable")
            
            // finish await
            self.expectation?.fulfill()
        }
        
        // arrange
        self.mockSuccessRequest()
        
        // act
        let taxiManager = TaxiManager(delegate: self)
        taxiManager.startGetTaxisLocationsForCoordinate(CLLocationCoordinate2D())
        
        // assert
        waitForExpectationsWithTimeout(1) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
                XCTFail("No response from API")
            }
        }
    }
    
    func testFailGetTaxisLocations() {
        self.finishRequestClosure = { (taxis:[Taxi]) -> Void in
            
            XCTAssertEqual(taxis.count, 0, "Expected an empty list")

            // finish await
            self.expectation?.fulfill()
        }
        
        // arrange
        self.mockErrorRequest()
        
        // act
        let taxiManager = TaxiManager(delegate: self)
        taxiManager.startGetTaxisLocationsForCoordinate(CLLocationCoordinate2D())
        
        // assert
        waitForExpectationsWithTimeout(1) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
                XCTFail("No response from API")
            }
        }
    }
    
    func testLocationUpdate(){
        var count = 0
        self.finishRequestClosure = { (taxis:[Taxi]) -> Void in
            count++
            // finish await
            if (count == 3){
                self.expectation?.fulfill()
            }
        }
        
        // arrange
        self.mockSuccessRequest()
        
        // act
        let taxiManager = TaxiManager(delegate: self)
        taxiManager.updateInverval = 0.1
        taxiManager.startGetTaxisLocationsForCoordinate(CLLocationCoordinate2D())
        
        // assert
        waitForExpectationsWithTimeout(3) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
                XCTFail("The location was not updated as expected")
            }
        }
        
    }
    
    func mockSuccessRequest(){
        OHHTTPStubs.stubRequestsPassingTest({$0.URL!.host == "api.99taxis.com"}) { _ in
            let fixture = OHPathForFile("lastLocations.json", self.dynamicType)
            return OHHTTPStubsResponse(fileAtPath: fixture!,
                statusCode: 200, headers: ["Content-Type":"application/json"])
        }
    }
    
    func mockErrorRequest(){
        OHHTTPStubs.stubRequestsPassingTest({$0.URL!.host == "api.99taxis.com"}) { _ in
            let fixture = OHPathForFile("lastLocationsError.html", self.dynamicType)
            return OHHTTPStubsResponse(fileAtPath: fixture!,
                statusCode: 500, headers: ["Content-Type":"text/html;charset=utf-8"])
        }
    }
    
    func didFinishFetchTaxis(taxis: [Taxi]) {
        self.finishRequestClosure?(taxis);
    }
    
    func didFailToFetchTaxisWithError(error: ErrorType?)
    {
        self.finishRequestClosure?([]);
    }
    
}
