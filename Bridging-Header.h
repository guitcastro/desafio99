//
//  Bridging-Header.h
//  Desafio99
//
//  Created by Guilherme Castro on 9/14/15.
//  Copyright © 2015 Guilherme Castro. All rights reserved.
//

#ifndef Bridging_Header_h
#define Bridging_Header_h


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "GClusterManager.h"
#import "NonHierarchicalDistanceBasedAlgorithm.h"
#import "GDefaultClusterRenderer.h"

#endif /* Bridging_Header_h */
