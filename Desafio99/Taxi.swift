//
//  Taxi.swift
//  Desafio99
//
//  Created by Guilherme Castro on 9/12/15.
//  Copyright © 2015 Guilherme Castro. All rights reserved.
//

import Foundation

public class Taxi{
    let driverId: Int
    var latitude: Double
    var longitude: Double
    var driverAvailable: Bool
    
    init(latitude: Double, longitude: Double, driverId: Int, driverAvailable: Bool){
        self.latitude = latitude
        self.longitude = longitude
        self.driverId = driverId
        self.driverAvailable = driverAvailable
    }
}