//
//  TaxiSpot.swift
//  Desafio99
//
//  Created by Guilherme Castro on 9/14/15.
//  Copyright © 2015 Guilherme Castro. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps


class TaxiSpot : NSObject, GClusterItem{
    @objc var position: CLLocationCoordinate2D
    @objc var marker: GMSMarker
    
    init(taxi: Taxi, marker: GMSMarker){
        self.position = CLLocationCoordinate2D(latitude: taxi.latitude, longitude: taxi.longitude)
        self.marker = marker
    }

}