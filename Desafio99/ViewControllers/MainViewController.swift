//
//  MainViewController.swift
//  Desafio99
//
//  Created by Guilherme Castro on 9/12/15.
//  Copyright © 2015 Guilherme Castro. All rights reserved.
//

import UIKit
import GoogleMaps

class MainViewController: UIViewController, TaxiManagerDelegate, CLLocationManagerDelegate {
    
    var mapView: GMSMapView?
    var locationManager: CLLocationManager?
    var markers: [Int : GMSMarker] = [ : ]
    var clusterManager: GClusterManager?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager?.requestAlwaysAuthorization()
        locationManager?.startUpdatingLocation()
        
        let camera = GMSCameraPosition.cameraWithLatitude(0, longitude: 0, zoom: 0)
        self.mapView = GMSMapView.mapWithFrame(CGRectZero, camera: camera)
        self.mapView?.myLocationEnabled = true
        self.mapView?.settings.myLocationButton = true
        self.view = mapView
        
        
        let renderer = GDefaultClusterRenderer(mapView: self.mapView)
        self.clusterManager = GClusterManager(mapView: self.mapView, algorithm: NonHierarchicalDistanceBasedAlgorithm(), renderer: renderer)
        mapView?.delegate = self.clusterManager
        //self.clusterManager?.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateCameraPosition(coordinate: CLLocationCoordinate2D){
        let camera = GMSCameraPosition.cameraWithTarget(coordinate, zoom: 16)
        
        self.mapView?.animateToCameraPosition(camera)
    }
    
    // MARK: TaxiManagerDelegate
    
    func didFinishFetchTaxis(taxis: [Taxi]) {
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.2)
        for taxi in taxis{
            var marker: GMSMarker? = self.markers[taxi.driverId]
            if (marker == nil ){
                marker = GMSMarker()
                marker?.icon = UIImage(named: "Pin-taxi")
                self.markers[taxi.driverId] = marker
                
                clusterManager?.addItem(TaxiSpot(taxi: taxi, marker: marker!))
            }
            marker?.position = CLLocationCoordinate2D(latitude: taxi.latitude, longitude: taxi.longitude)
        }
        self.clusterManager?.cluster()
        CATransaction.commit()
    }
    
    func didFailToFetchTaxisWithError(error: ErrorType?) {
        
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        if let location = locations.last{
            self.locationManager?.stopUpdatingLocation()
            self.updateCameraPosition(location.coordinate)
            
            let marker = GMSMarker()
            marker.position = location.coordinate
            marker.map = self.mapView
            marker.icon = UIImage(named: "Pin-passenger-down")
            
            let taxiManager = TaxiManager(delegate: self)
            taxiManager.startGetTaxisLocationsForCoordinate(location.coordinate)
        }
        
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError){
        debugPrint(error.description)
    }

}
