//
//  TaxiManager.swift
//  Desafio99
//
//  Created by Guilherme Castro on 9/12/15.
//  Copyright © 2015 Guilherme Castro. All rights reserved.
//

import Foundation
import Alamofire
import CoreLocation

public protocol TaxiManagerDelegate{
    func didFinishFetchTaxis(taxis : [Taxi])
    func didFailToFetchTaxisWithError(error: ErrorType?)
}

class TaxiManager {
    
    let delegate: TaxiManagerDelegate
    var timer: NSTimer?
    var updateInverval :NSTimeInterval = 2.0
    
    var northEast: CLLocationCoordinate2D = CLLocationCoordinate2D.init()
    var southWest: CLLocationCoordinate2D = CLLocationCoordinate2D.init()
    
    init(delegate: TaxiManagerDelegate){
        self.delegate = delegate
    }
    
    func startGetTaxisLocationsForCoordinate(coordinate: CLLocationCoordinate2D){
        let delta = 0.02
        self.southWest = CLLocationCoordinate2D(latitude: coordinate.latitude - delta, longitude: coordinate.longitude - delta)
        self.northEast = CLLocationCoordinate2D(latitude: coordinate.latitude + delta, longitude: coordinate.longitude + delta)
        self.updateTaxisLocations()
        timer = NSTimer.scheduledTimerWithTimeInterval(updateInverval, target: self, selector: "updateTaxisLocations", userInfo: nil, repeats: true)
    }
    
    @objc private func updateTaxisLocations(){
        let sw = "\(self.southWest.latitude),\(self.southWest.longitude)"
        let ne = "\(self.northEast.latitude),\(self.northEast.longitude)"
        Alamofire.request(.GET, "https://api.99taxis.com/lastLocations", parameters: ["sw": sw , "ne" : ne])
            .responseJSON { response in
                switch (response.result) {
                case .Failure(let error):
                    debugPrint(response)
                    self.delegate.didFailToFetchTaxisWithError(error)
                    break;
                case .Success(let json):
                    let taxis = self.parseTaxis(json as! [Dictionary<String,AnyObject>])
                    self.delegate.didFinishFetchTaxis(taxis)
                }
        }
    }
    
    private func parseTaxis(jsonArray: [Dictionary<String,AnyObject>]) -> [Taxi]{
        var taxis = [Taxi]()
        for json in jsonArray{
            let driverId = json["driverId"] as? Int
            let latitude = json["latitude"] as? Double
            let longitude = json["longitude"] as? Double
            let driverAvailable = json["driverAvailable"] as? Bool
            if (latitude != nil && longitude != nil && driverId != nil && driverAvailable != nil){
                let taxi = Taxi(latitude: latitude!, longitude: longitude!, driverId: driverId!, driverAvailable: driverAvailable!)
                taxis.append(taxi)
            }
        }
        return taxis
    }
    
}