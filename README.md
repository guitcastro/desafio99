# Desafio99

## About

Projeto Aplicativo Android ou iOS Taxis

Objetivo
Implementar um aplicativo que mostre um mapa com os taxistas ativos da 99Taxis.

Objetivos Bonus:
Atualizar as posições quando apertar um botão; atualizar as posições de tempos em tempos; colocar um ícone não padrão pro pin do mapa; animar as posições; marcar a posição do usuário (GPS) no mapa; agrupar posições que estejam muito perto; mostrar id do taxista quando clicar no icone dele no mapa; use a criatividade e faça o seu bonus ;)

API
Lista os taxistas ativos dentro de uma retângulo geográfico

Endpoint: 
GET https://api.99taxis.com/lastLocations

Parâmetros:
- sw: Ponto extremo sul, extremo oeste do retângulo, no formato "latitude,longitude". Ex: -23.612474,-46.702746
- ne: Ponto extremo norte, extremo leste do retângulo, no formato "latitude,longitude". Ex: -23.589548,-46.673392

Resposta:
Um array em formato json, de um objeto com atributos:
- latitude
- longitude
- driverId (id único de taxista no sistema)
- driverAvailable: true/false. Representa se o taxista está disponível ou não para corridas. Neste endpoint retorna sempre true.


## Compile prerequisites

* Xcode 7.1 Beta
* Cocoa Pods

## Compile instructions

* Before open the project run `pod install` to update the pods
* After that make sure you opened `Desafio99.xcworkspace` and not `Desafio99.xcodeproj`

